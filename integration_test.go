// +build integration

package slack_test

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/cvvs/slack"
	"github.com/cvvs/slack/message"
)

const (
	webhookEnvVar            = "TEST_WEBHOOK_URL"
	webhookTestChannelEnvVar = "TEST_WEBHOOK_CHANNEL"
)

func TestSendWebhook(t *testing.T) {
	testURLOK := os.Getenv(webhookEnvVar)
	if testURLOK == "" {
		t.Skip("set the '" + webhookEnvVar + "' environment variable to enable webhook integration tests")
	}

	testChannel := os.Getenv(webhookTestChannelEnvVar)
	if testChannel == "" {
		t.Skip("set the '" + webhookTestChannelEnvVar + "' environment variable to enable webhook integration tests")
	}

	client := http.DefaultClient
	client.Timeout = 30 * time.Second

	var msg message.Message
	msg.Channel = testChannel
	msg.Username = "CVVS Slack Integration Test"
	msg.IconEmoji = ":large_blue_diamond:"

	field1 := message.Field{Title: "Field 1", Value: "1", Short: true}
	field2 := message.Field{Title: "Field 2", Value: "2", Short: true}
	field3 := message.Field{Title: "A much longer field", Value: "with a much longer value"}

	buttonOne := message.LinkButton("Bing", "https://www.bing.com", message.DefaultButtonStyle)
	buttonTwo := message.LinkButton("Example", "http://www.example.com", message.DangerButtonStyle)

	var attachment message.Attachment
	attachment.Fallback = "Some fields and some link buttons"
	attachment.Fields = []message.Field{field1, field2, field3}

	if err := attachment.AddAction(buttonOne); err != nil {
		t.Error("error adding link button 1:", err)
	}

	if err := attachment.AddAction(buttonTwo); err != nil {
		t.Error("error adding link button 2:", err)
	}

	msg.Attachments = []*message.Attachment{&attachment}

	bites, err := json.Marshal(&msg)
	if err != nil {
		t.Log("raw_message:", string(bites))
		t.Fatal("failed to marshall message:", err)
	}

	resp, err := slack.SendWebhook(context.Background(), client, testURLOK, &msg)
	if err != nil {
		t.Error("error sending webook:", err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("response code %d != %d", resp.StatusCode, http.StatusOK)
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatal("error reading response body:", err)
		}
		t.Log("response body:", string(body))
	}
}
