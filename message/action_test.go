package message_test

import (
	"encoding/json"
	"testing"

	"github.com/cvvs/slack/message"
)

//Link Button Tests
func TestLinkButtonValidate(t *testing.T) {

	attachment := message.Attachment{
		Fallback: "Some Fallback Content",
	}

	linkbutton := message.LinkButton("Some Text Content", "http://www.example.com", "")

	if err := linkbutton.Validate(&attachment); err != nil {
		t.Error("validate should not have thrown an error")
	}
}

func TestLinkButtonValidateFail(t *testing.T) {

	url := "http://www.example.com"
	fallback := "Some Fallback Content"
	text := "Some Text Content"

	attachment := message.Attachment{
		Fallback: "",
	}

	linkbutton := message.LinkButton(text, url, message.DefaultButtonStyle)

	//Test without fallback
	attachment.Fallback = ""
	if err := linkbutton.Validate(&attachment); err == nil {
		t.Error("empty fallback should have errored")
	}
	attachment.Fallback = fallback

	//Test without Text
	linkbutton = message.LinkButton("", url, message.DefaultButtonStyle)
	if err := linkbutton.Validate(&attachment); err == nil {
		t.Error("empty text should have errored")
	}

	//Test without URL
	linkbutton = message.LinkButton(text, "", message.DefaultButtonStyle)
	if err := linkbutton.Validate(&attachment); err == nil {
		t.Error("nil url should have errored")
	}

}

//Mock to test Marshalling
type mockLinkButton struct {
	Text  string `json:"text"`
	URL   string `json:"url"`
	Style string `json:"style"`
	Type  string `json:"type"`
}

func TestLinkButtonMarshal(t *testing.T) {
	text := "TestLinkButtonMarshal"
	link := "http://www.example.com"
	style := message.DangerButtonStyle

	var button = message.LinkButton(text, link, style)

	bites, err := json.Marshal(button)
	if err != nil {
		t.Fatal("error marshalling linkbutton:", err)
	}

	var mlb mockLinkButton
	if err := json.Unmarshal(bites, &mlb); err != nil {
		t.Log("raw:", string(bites))
		t.Fatal("error unmarshalling mock:", err)
	}

	if mlb.Style != string(style) {
		t.Errorf("button style '%s' != '%s'", mlb.Style, style)
	}

	if mlb.Text != text {
		t.Errorf("button text '%s' != '%s'", mlb.Text, text)
	}

	if mlb.URL != link {
		t.Errorf("button url '%s' != '%s'", mlb.URL, link)
	}

	if mlb.Type != string(button.ActionType()) {
		t.Log("raw:", string(bites))
		t.Errorf("button type '%s' != '%s'", mlb.Type, button.ActionType())
	}
}

//Action Button Tests
func TestActionButtonValidate(t *testing.T) {
	name := "TextActionButton"
	text := "ActionText"
	value := "ActionValue"
	style := message.DangerButtonStyle
	config := message.ConfirmDialog{Text: "Confirm"}

	attachment := message.Attachment{
		Fallback:   "Some Fallback Content",
		CallbackID: "cid_12345",
	}

	button := message.ActionButton(name, text, value, style, &config)

	if err := button.Validate(&attachment); err != nil {
		t.Error("action button validation failed:", err)
	}

}

func TestActionButtonValidateFail(t *testing.T) {
	name := "TextActionButton"
	text := "ActionText"
	value := "ActionValue"
	style := message.PrimaryButtonStyle
	config := message.ConfirmDialog{Text: "Confirm"}

	attachment := message.Attachment{
		Fallback:   "Some Fallback Content",
		CallbackID: "cid_12345",
	}

	button := message.ActionButton("", text, value, style, &config)
	if err := button.Validate(&attachment); err == nil {
		t.Error("empty 'name' should have thrown an error")
	}

	button = message.ActionButton(name, "", value, style, &config)
	if err := button.Validate(&attachment); err == nil {
		t.Error("emtpy 'text' should have thrown an error")
	}

	attachment = message.Attachment{
		Fallback:   "",
		CallbackID: "cid_12345",
	}
	button = message.ActionButton(name, text, value, style, &config)
	if err := button.Validate(&attachment); err == nil {
		t.Error("emtpy attachment 'fallback' should have thrown an error")
	}

	attachment = message.Attachment{
		Fallback:   "Some Fallback Content",
		CallbackID: "",
	}
	button = message.ActionButton(name, text, value, style, &config)
	if err := button.Validate(&attachment); err == nil {
		t.Error("emtpy attachment 'callback_id' should have thrown an error")
	}
}

//Mock to test Marshalling
type mockActionButton struct {
	Name    string                 `json:"name"`
	Text    string                 `json:"text"`
	Type    string                 `json:"type"`
	Value   string                 `json:"value,omitempty"`
	Confirm *message.ConfirmDialog `json:"confirm,omitempty"`
	Style   string                 `json:"style,omitempty"`
}

//TODO: Not really necesssary since we use standard marshaller, now. But, I'm not sure
//it needs removed. -CVVS
func TestActionButtonMarshal(t *testing.T) {
	name := "TextActionButton"
	text := "ActionText"
	value := "ActionValue"
	style := message.DangerButtonStyle
	confirm := message.ConfirmDialog{Text: "Confirm"}

	var button = message.ActionButton(name, text, value, style, nil)

	bites, err := json.Marshal(button)
	if err != nil {
		t.Fatal("error marshalling linkbutton:", err)
	}
	var mab mockActionButton
	if err := json.Unmarshal(bites, &mab); err != nil {
		t.Log("raw:", string(bites))
		t.Fatal("error unmarshalling mock:", err)
	}

	if mab.Confirm != nil {
		t.Errorf("button Confirm '%v' != '%v'", mab.Confirm, nil)
	}

	if mab.Name != name {
		t.Errorf("button Name '%s' != '%s'", mab.Name, name)
	}

	if mab.Style != string(style) {
		t.Errorf("button Style '%s' != '%s'", mab.Style, style)
	}

	if mab.Text != text {
		t.Errorf("button Text '%s' != '%s'", mab.Text, text)
	}

	if mab.Value != value {
		t.Errorf("buton Value '%s' != '%s'", mab.Value, value)
	}

	if mab.Type != string(button.ActionType()) {
		t.Log("raw:", string(bites))
		t.Errorf("button Type '%s' != '%s'", mab.Type, button.ActionType())
	}

	button = message.ActionButton(name, text, value, style, &confirm)

	bites, err = json.Marshal(button)
	if err != nil {
		t.Fatal("error marshalling linkbutton:", err)
	}

	if err := json.Unmarshal(bites, &mab); err != nil {
		t.Log("raw:", string(bites))
		t.Fatal("error unmarshalling mock:", err)
	}

	if mab.Confirm == nil {
		t.Fatalf("confirm dialog should not be nil")
	}

	//TODO: This section probably should be in the ConfirmDiaglog validation.
	if mab.Confirm.Title != confirm.Title {
		t.Errorf("confirm Title '%s' != '%s'", mab.Confirm.Title, confirm.Title)
	}

	if mab.Confirm.DismissText != confirm.DismissText {
		t.Errorf("confirm DismissText '%s' != '%s'", mab.Confirm.DismissText, confirm.DismissText)
	}

	if mab.Confirm.OKText != confirm.OKText {
		t.Errorf("confirm OKText '%s' != '%s'", mab.Confirm.OKText, confirm.OKText)
	}

	if mab.Confirm.Text != confirm.Text {
		t.Errorf("confirm text '%s' != '%s'", mab.Confirm.Text, confirm.Text)
	}
}
