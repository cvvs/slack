package message_test

import (
	"testing"

	"github.com/cvvs/slack/message"
)

func TestAddAction(t *testing.T) {
	text := "Some Text"
	link := "http://www.example.com"

	action := message.LinkButton(text, link, message.DefaultButtonStyle)

	var attachment message.Attachment
	attachment.Fallback = "Some Fallback" // Required to pas validation

	for i := 0; i < message.MaxActionsPerAttachment; i++ {
		if err := attachment.AddAction(action); err != nil {
			t.Error("unexpected error adding attachment:", err)
		}
	}

	if err := attachment.AddAction(action); err == nil {
		t.Error("expected error adding attachment >", message.MaxActionsPerAttachment)
	} else {
		if _, ok := err.(message.ErrMaxActionsExceeded); !ok {
			t.Errorf("expected error type '%T' != '%T'", err, message.ErrMaxActionsExceeded(0))
		}
	}
}
