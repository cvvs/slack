package message

import (
	"net/url"
)

//Built-in colors for slack
const (
	ColorGood    = "good"
	ColorWarning = "warning"
	ColorDanger  = "danger"
)

const (
	//MaxAttachmentsPerMessage the maximim number of attachments Slack allows per message
	MaxAttachmentsPerMessage = 400
	//MaxActionsPerAttachment the maximum number of acctions Slack allows per attachment
	MaxActionsPerAttachment = 5
)

//Message represents a slack message
type Message struct {
	Text        string        `json:"text"`
	Username    string        `json:"username,omitempty"`
	IconEmoji   string        `json:"icon_emoji,omitempty"`
	Channel     string        `json:"channel,omitempty"`
	Attachments []*Attachment `json:"attachments,omitempty"`
}

//Attachment is a Slack message attachment. Either Text or Fallback must be provided
type Attachment struct {
	Fallback   string   `json:"fallback,omitempty"` //Fallback is a plaintext with no formatting for less rich clients
	Color      string   `json:"color,omitempty"`
	Pretext    string   `json:"pretext,omitempty"`
	AuthorName string   `json:"author_name,omitempty"`
	AuthorLink string   `json:"author_link,omitempty"`
	AuthorIcon string   `json:"author_icon,omitempty"`
	Title      string   `json:"title,omitempty"`
	TitleLink  url.URL  `json:"title_link,omitempty"`
	Text       string   `json:"text,omitempty"`
	Fields     []Field  `json:"fields,omitempty"`
	ImageURL   url.URL  `json:"image_url,omitempty"`
	ThumbURL   url.URL  `json:"thumb_url,omitempty"`
	Footer     string   `json:"footer,omitempty"`
	FooterIcon string   `json:"footer_icon,omitempty"` //If Footer isn't defined, then this value is ignored
	TimeStamp  int64    `json:"ts,omitempty"`          //Unix timestamp
	CallbackID string   `json:"callback_id,omitempty"`
	Actions    []Action `json:"actions,omitempty"`
}

//AddAction add and validate an Action to an attachment
func (attachment *Attachment) AddAction(action Action) error {
	var err error
	if len(attachment.Actions) < MaxActionsPerAttachment {
		err = action.Validate(attachment)
		if err == nil {
			attachment.Actions = append(attachment.Actions, action)
		}
	} else {
		err = ErrMaxActionsExceeded(MaxActionsPerAttachment)
	}
	return err
}

//Field is an attachment field
type Field struct {
	Title string `json:"title"`
	Value string `json:"value"`
	Short bool   `json:"short,omitempty"`
}

//ConfirmDialog represents a confirm action in a message
type ConfirmDialog struct {
	Title       string `json:"title"`
	Text        string `json:"text"`
	OKText      string `json:"ok_text"`
	DismissText string `json:"dismiss_text"`
}

//Validate that a confirm dialog has all the required fields.
func (dialog ConfirmDialog) Validate() error {
	var err error
	if dialog.Text == "" {
		err = ErrValidationFail("dialog text not defined")
	}
	return err
}
