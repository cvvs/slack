package message

import (
	"strings"
)

const (
	//LinkButtonAction creates a link only button in a slack attachment
	LinkButtonAction ActionType = "button"
	//ButtonAction creates a button action in a slack attachement
	ButtonAction ActionType = "button"
	//MenuAction creates a menu action in a slack attachment
	MenuAction ActionType = "menu"
	//PrimaryButtonStyle sets the Button to green
	PrimaryButtonStyle ButtonStyle = "primary"
	//DangerButtonStyle sets the Button to red
	DangerButtonStyle ButtonStyle = "danger"
	//DefaultButtonStyle sets the button to default
	DefaultButtonStyle ButtonStyle = ""
)

//ButtonStyle is a Slack button Style
type ButtonStyle string

//ActionType not sure why, but whatevah
type ActionType string

//Action is a Slack action
type Action interface {
	ActionType() ActionType
	Validate(*Attachment) error
}

//LinkButton creates a Slack link button
func LinkButton(text, link string, style ButtonStyle) Action {
	lb := new(linkButton)
	lb.Type = LinkButtonAction
	lb.Text = text
	lb.URL = link
	lb.Style = style
	return lb
}

//linkButton is a link button action in a Slack attachment
type linkButton struct {
	Text  string      `json:"text"`
	URL   string      `json:"url"`
	Style ButtonStyle `json:"style,omitempty"`
	Type  ActionType  `json:"type"`
}

//ActionType returns the action type for this action
func (action linkButton) ActionType() ActionType {
	return LinkButtonAction
}

//Validate validates that the action can be added to the attachment
func (action linkButton) Validate(attachment *Attachment) error {

	fails := make([]string, 0, 4)
	if action.Text == "" {
		fails = append(fails, "action text cannot be empty")
	}
	if action.URL == "" {
		fails = append(fails, "url cannot be empty")
	}
	if attachment.Fallback == "" {
		fails = append(fails, "fallback cannot be empty")
	}
	if action.Type != LinkButtonAction {
		fails = append(fails, "type not specified")
	}

	var err error
	if len(fails) > 0 {
		err = ErrValidationFail("link button validation: " + strings.Join(fails, ","))
	}

	return err
}

//NewActionButton returns a Slack Button. Link buttons should use NewLinkButton
func ActionButton(name, text, value string, style ButtonStyle, confirm *ConfirmDialog) Action {
	b := new(actionButton)
	b.Name = name
	b.Text = text
	b.Value = value
	b.Confirm = confirm
	b.Type = ButtonAction
	b.Style = style
	return b
}

//Button is a slack attachment button
type actionButton struct {
	Name    string         `json:"name"`
	Text    string         `json:"text"`
	Type    ActionType     `json:"type"`
	Value   string         `json:"value,omitempty"`
	Confirm *ConfirmDialog `json:"confirm,omitempty"`
	Style   ButtonStyle    `json:"style,omitempty"`
}

//ActionType of the action
func (action *actionButton) ActionType() ActionType {
	return action.Type
}

//Validate an action being added to an attachment
func (action *actionButton) Validate(attachment *Attachment) error {
	fails := make([]string, 0, 6)

	//TODO: An empty string may be allowable, and if so, simply removing ',omitempty' may be good enough
	if action.Name == "" {
		fails = append(fails, "name cannot be empty")
	}

	if action.Type != ButtonAction {
		fails = append(fails, "type not specified")
	}

	//TODO: An empty string may be allowable, and if so, simply removing ',omitempty' may be good enough
	if action.Text == "" {
		//We may just need text or fallback
		fails = append(fails, "action text cannot be empty")
	}

	//TODO: An empty string may be allowable, and if so, simply removing ',omitempty' may be good enough
	//We may just need Text OR fallback
	if attachment.Fallback == "" {
		fails = append(fails, "fallback cannot be empty")
	}

	//TODO: An empty string may be allowable, and if so, simply removing ',omitempty' may be good enough
	if attachment.CallbackID == "" {
		fails = append(fails, "callback id cannot be empty")
	}

	if action.Confirm != nil {
		if e := action.Confirm.Validate(); e != nil {
			fails = append(fails, "confirm validation: "+e.Error())
		}
	}

	var err error
	if len(fails) > 0 {
		err = ErrValidationFail("link button validation: " + strings.Join(fails, ","))
	}

	return err
}
