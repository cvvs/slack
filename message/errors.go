package message

//ErrMaxActionsExceeded is raised when the too many actions are added to an attachment
type ErrMaxActionsExceeded int

func (e ErrMaxActionsExceeded) Error() string {
	return "max actions exceeded"
}

//ErrValidationFail is returned When an message validation failes
type ErrValidationFail string

func (e ErrValidationFail) Error() string {
	return string(e)
}
