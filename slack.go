package slack

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"

	"github.com/cvvs/slack/message"
)

const (
	contentTypeHeader = "Content-Type"
	jsonContent       = "application/json"
)

//SendWebhook sends a slack message to the provided webhook using the provided http.Client
func SendWebhook(ctx context.Context, client *http.Client, webhookURL string, msg *message.Message) (*http.Response, error) {
	bites, err := json.Marshal(msg)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, webhookURL, bytes.NewReader(bites))
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	req.Header.Set(contentTypeHeader, jsonContent)

	return client.Do(req)
}
