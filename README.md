# Simple Slack Message Lib

A simple library used to create Slack messages. Currently, only a subset of message features have been implemented, and
only only the basics have been tested via Slack.

# TODOS
- Implement menu
- Real integration tests vs current human inspection
- Improved unit tests